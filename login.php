<?php
// start or resume session
if (!isset($_SESSION)) {
    session_start();
}
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('config/initialise.php');
require_once('config/config.php');
require_once('config/debug.inc.php');

// load page parts
$currentPage = "The Hub Sign In";

// Clear error message
$errorMsg = "";


if(isset($_POST["submit"])){
  
  //test for userName and passPhrase
  if(empty($_POST['userName'])){
    $errorMsg = "No user name";
  }
  if(empty($_POST['passPhrase'])){
    $errorMsg = "No password";
  }
  
  $userName = $_POST["userName"];
  $passPhrase = $_POST["passPhrase"];
  $realName = "";
  
  $find_command = $fm->newFindCommand('tbl_user');
  $find_command->addFindCriterion('userName', $userName);
  $find_command->addFindCriterion('passPhrase', $passPhrase);
  $result = $find_command->execute();
  
  // If an error is found, return a message and exit.
    if (FileMaker::isError($result)) {
    $errorMsg = $result->getMessage();

    } else {
    foreach ($result->getRecords() as $row) {
        $_SESSION['realName'] = $row->getField('realName');
        $_SESSION['userName'] = $userName;
        $_SESSION['passPhrase'] = $passPhrase;
        $_SESSION['lastContactSearch'] = $row->getField('lastContactSearch');
        $_SESSION['lastProjectSearch'] = $row->getField('lastProjectSearch');
        $userid = $row->getField('pk_userID');
        $_SESSION['userId'] = $row->getField('pk_userID');
        $_SESSION['userRecId'] = $row->getRecordId();
        $_SESSION['timeSheetUser'] = $row->getField('timeSheetUser');
        header("location: index.php");
        exit;
    }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Login for The Hub</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php
            if(!empty($errorMsg)){
              echo '<p class="bg-danger">'.$errorMsg.'</p>';
            }
        ?>
        <label for="userName" class="sr-only">User Name</label>
        <input type="username" name="userName" class="form-control" placeholder="User Name" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="passPhrase" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" >Sign in</button>
      </form>
      
      
      
      
    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>