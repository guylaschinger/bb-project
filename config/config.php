<?php
// test for nasty access
if (!defined('ENVIRONMENT')) exit("No direct access allowed! 2");

// define the standard user credentials
define("fmUserName", "web");
define("fmPassword", "web");

// Include the filemaker library
require_once("FileMaker.php");

// Set up a connection
$fm = new FileMaker();
$fm->setProperty('database', 'Hub_Web');
$fm->setProperty('hostspec', 'localhost');
$fm->setProperty('username', 'web');
$fm->setProperty('password', 'web');