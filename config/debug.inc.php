<?php
// Make sure its not run independently
if (!defined('ENVIRONMENT')) exit("No direct access allowed!");

function dump_this($what) {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<pre>';
        if (is_array($what)) {
            print_r($what);
        } else {
            var_dump($what);
        }
        echo '</pre>';
    }
}

function debug_dump_SESSION() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<h2>$_SESSION:</h2>';
        echo "<PRE>";
        print_r($_SESSION);
        echo "</PRE>";
        echo "<BR/>";
    }
}

function debug_dump_SERVER() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<h2>$_SERVER:</h2>';
        echo "<PRE>";
        print_r($_SERVER);
        echo "</PRE>";
        echo "<BR/>";
    }
}

function debug_dump_POST() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<h2>$_POST:</h2>';
        echo "<PRE>";
        print_r($_POST);
        echo "</PRE>";
        echo "<BR/>";
    }
}

function debug_dump_GET() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<h2>$_GET\[]:</h2>';
        echo "<PRE>";
        print_r($_GET);
        echo "</PRE>";
        echo "<BR/>";
    }
}

function debug_dump_ALL() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        debug_dump_GET();
        debug_dump_POST();
        debug_dump_SERVER();
    }
}

function debug_all_defined() {
    if (defined('ENVIRONMENT') && ENVIRONMENT == 'debug') {
        echo '<h2>ALL DEFINED VARS:</h2>';
        echo "<PRE>";
        print_r(get_defined_vars());
        echo "</PRE>";
        echo "<BR/>";
    }
}
