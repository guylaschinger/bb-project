<?php
// test for nasty access
if (!defined('ENVIRONMENT')) exit("No direct access allowed! 2");

if (!isset($_SESSION)){
    session_start();
}

// define the standard user credentials
$fmUserName = $_SESSION['userName'];
$fmPassword = $_SESSION['passPhrase'];
define("fmUserName", $fmUserName);
define("fmPassword", $fmPassword);

// Include the filemaker library
require_once("FileMaker.php");

// Set up a connection
$fm = new FileMaker();
$fm->setProperty('database', 'Hub_Web');
$fm->setProperty('hostspec', 'localhost');
$fm->setProperty('username', $fmUserName);
$fm->setProperty('password', $fmPassword);