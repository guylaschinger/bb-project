<?php
// Make sure its not run independently
if ( !defined('ENVIRONMENT') ) exit("No direct access allowed! 1");


// INIT - Error reporting
if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
        case 'debug' :
            error_reporting(E_ALL);
            break;

        case 'development':
			error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
			break;
		
		case 'testing':
		case 'production':
			error_reporting(0);
			break;

		default:
			exit('The application environment is not set correctly.');
	}
}