<?php
// start or resume session
if (!isset($_SESSION)) {
    session_start();
}

// FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS 
// value list handler function
function printDropList($layoutName, $valuelistName, $listName, $recId) {
  global $fm, $record;
  $layoutObj = $fm->getLayout($layoutName);
  $valuearray = $layoutObj->getValueListTwoFields($valuelistName,$recId);
  $out = '<select name="'. $listName .'" >';
  foreach ($valuearray as $displayValue => $value) {
    $out .= '<option value="' . $value . '">' . $displayValue . '</option>';
  }
  $out .= '</select>';
  echo $out;
}
// date handler function
function toggleDMY2MDY($datein){
    if(strlen($datein)>0){
        $parts=explode('/',$datein);
        return$parts[1].'/'.$parts[0].'/'.$parts[2];
    }
}
// complete status display function
function completeStatus($completeStatus){
    if($completeStatus == 1)
        $statusDisplay = 'Completed';
        echo $statusDisplay;
}
// expense display function
function expenseSheetRowDisplay($expenseStatus, $expenseRowClass, $expenseRowName, $expenseValue){
    if( $expenseStatus == 1 ) {
        $expenseSheetRowDisplay =  '<td class="' . $expenseRowClass . '">'. $expenseValue .'</td>';
    } else {
        $expenseSheetRowDisplay = '<td class="' . $expenseRowClass . '"><input type="text" id="'. $expenseRowName .'" name="'. $expenseRowName .'[]" value="'. $expenseValue .'"></td>';
            }
                return $expenseSheetRowDisplay;
        }

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');
$currentModule = "";

// load page parts
$currentPage = "Expense";
$currentModule = 5;
$recId = '';
require_once("inc/header.inc.php");
require_once("inc/navbar.inc.php");

$recId = null;
if(isset($_GET['recId'])){
    $recId = $_GET['recId'];
}elseif(isset($_POST['recId'])){
    $recId = $_POST['recId'];
}else($recId = null);

// Clear error message
$errorMsg = "";

//--------------------expenseList------------------------------------------------
if($recId == null){
	$findCommand =& $fm->newFindCommand('tbl_expense');
    $findCommand->addFindCriterion('fk_userID', $userId);
    $findCommand->addSortRule('pk_expenseID', 1, FILEMAKER_SORT_DESCEND);
    $record = $findCommand->execute();
    if (FileMaker::isError($record)) {
    echo "<body>Error: " . $record->getMessage(). "</body>";
    exit;
    }
    ?>
    	<div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
            <th>No.</th>
            <th>Date</th>
            <th class="th-expenseValues">Expense Total</th>
            <th>Completed On</th>
            <th>Date Paid</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($record->getRecords() as $records){
                echo "<tr>";
                echo "<td><a href=\"".$_SERVER['PHP_SELF']."?recId=" . $records->getRecordId() . "\">" . $records->getField('pk_expenseID') . "</td>";
                echo "<td>" . toggleDMY2MDY($records->getField('expenseSheetDate')) . "</td>";
                echo "<td class=\"td-expenseValues\">" . "&pound ". number_format($records->getField('expenseTotal') ,2,'.',',') . "</td>";
                echo "<td>" . toggleDMY2MDY($records->getField('completionDate')) . "</td>";
                echo "<td>" . toggleDMY2MDY($records->getField('datePaid')) . "</td>";
                echo "</tr>";
                }
            ?>
        </tbody>
        </table>
    </div>

<?php

//--------------------expenseForm------------------------------------------------
}else{
	$recId = $_GET['recId'];
    $record = $fm->getRecordById('tbl_expense', $_GET['recId']);
        if (FileMaker::isError($record)) {
        echo "  <body>Error: " . $record->getMessage(). "</body>";
        exit;
    }
    foreach ($record as $records) {
                $expenseID = $records->getField('pk_expenseID');
                $userID = $records->getField('fk_userID');
                $userName = $records->getField('tbl_expense|user::realName');
                $expenseStatus = $records->getField('expenseStatus');
                $date = $records->getField('expenseSheetDate');
                $signOffDate = $records->getField('signOffDate');
                $completionDate = $records->getField('completionDate');
                $datePaid = $records->getField('datePaid');
                $expenseTotal = $records->getField('expenseTotal'); 
                $expenseSheetRowCount = $records->getField('expenseSheetRowCount');
            }
            ?>
<!-- Time Sheet Detail-->
<div class="row">
    <!-- leftBox -----------------------------      -->
    <div class="col-md-6">
        <?php echo "ExpenseSheet Date: " . toggleDMY2MDY($records->getField('expenseSheetDate')) ?><br>
        <?php echo "Name: {$userName}"; ?><br>
    </div>
    <!-- rightBox -->
    <div class="col-md-6">
        <strong><?php echo "ExpenseSheet No.: {$expenseID}"; ?></strong><br>
        <?php echo "Completion Date: " . toggleDMY2MDY($records->getField('completionDate')) ?><br>
    </div>      
</div> 
<div class="table-responsive">
	<form id="expense-form" action="inc/datahandler_expense.php" method="post">
      <table class="table table-condensed">
      	<thead>
          <tr>
            <th>Date</th>
            <th>Project</th>
            <th>Expense Type</th>
            <th>Description</th>
            <th>Qty</th>
            <th>Cost</th>
            <th>Amount</th>
            <th>Receipt</th>
        </tr>
        </thead>
        <tbody>
        	<?php
        		if($expenseSheetRowCount > 0){
        			$findCommand =& $fm->newFindCommand('tbl_expenselineitem');
	                $findCommand->addFindCriterion('fk_expenseID', $expenseID);
	                $record = $findCommand->execute();
	                if (FileMaker::isError($record)) {
	                echo "<body>Error: " . $record->getMessage(). "</body>";
	                exit;
	            	}
	            		foreach($record->getRecords() as $records){
                            $projectDescription = $records->getField('tbl_expense|project::jobNumberNameDisplay');
                            $expenseType = $records->getField('tbl_expense|expensetype::expenseType');
                            $expenseDate = $records->getField('expenseDate');
                            $qty = $records->getField('qty');
                            $unitCost = $records->getField('unitCost');
                            if(empty($unitCost)){$unitCost = 0;}
                            $description = $records->getField('description');
                            $amount = $records->getField('amount');
                            if(empty($amount)){$amount = 0;}
                            $expenseTypeID;

                		echo "<tr>";
                		echo '<td class="expenseDate">' . toggleDMY2MDY($records->getField('expenseDate')) . "</td>";
                        if($amount > 0){
                                echo "<td>{$projectDescription}</td>";
                            }else{
                                echo '<td class="projectDescription" id="fk_projectID" name="fk_projectID[]">';
                                printDropList('tbl_expense', 'expenseSheetProjects', 'fk_projectID[]', $recId);
                            }
		                if($amount > 0){
                                echo "<td>{$expenseType}</td>";
                            }else{
                                echo '<td class="expenseValues" id="expenseTypeID" name="expenseTypeID[]">';
                                printDropList('tbl_expense', 'expenseTypeList', 'expenseTypeID[]', $recId);
                            }
                		echo "</td>";
                        echo expenseSheetRowDisplay($expenseStatus, 'expenseDescription', 'expenseDescription', $description);
                        echo expenseSheetRowDisplay($expenseStatus, 'td-narrow', 'expenseQty', $qty);
                        echo expenseSheetRowDisplay($expenseStatus, 'td-narrow', 'expenseUnitCost', number_format($unitCost,2,'.',','));
                        echo "<td class=\"td-narrow-right\">" . number_format($amount,2,'.',',') . "</td>";
                        //echo '<td class="deleteBox"><input type="checkbox" id="delete" name="delete_'.$rowRecId.'" value="1"></td>';
                        echo '<td><input type="checkbox" name="receipt" value="1"></td>';
                		echo "</tr>";
                		}
                        if($expenseStatus == 0){
                            echo "<tr>";
                            echo '<td class="expenseDate"><input type="text" id="expenseDate" name="expenseDate[]" value=""></td>';
                            echo '<td class="projectDescription" id="fk_projectID" name="fk_projectID[]">';
                            printDropList('tbl_expense', 'expenseSheetProjects', 'fk_projectID[]', $recId);
                            echo '<td class="expenseValues" id="expenseTypeID" name="expenseTypeID[]">';
                            printDropList('tbl_expense', 'expenseTypeList', 'expenseTypeID[]', $recId);
                            echo "</td>";
                            echo '<td class="expenseDescription"><input type="text" id="expenseDescription" name="expenseDescription[]" value=""></td>';
                            echo '<td class="td-narrow"><input type="text" id="expenseQty" name="expenseQty[]" value=""></td>';
                            echo '<td class="td-narrow"><input type="text" id="expenseUnitCost" name="expenseUnitCost[]" value=""></td>';
                            echo "<td>&nbsp</td>";
                            echo '<td><input type="checkbox" name="receipt" value="1"></td>';
                            echo "</tr>";
                        }
        		}
        	?>
        </tbody>
        <tfoot>
            <tr>
                <th>&nbsp</th>
                <th>&nbsp</th>
                <th>&nbsp</th>
                <th>&nbsp</th>
                <th>&nbsp</th>
                <th>&nbsp</th>
                <th><?php echo "£ ".number_format($expenseTotal,2,'.',',')  ?></th>
                <th>&nbsp</th>
            </tr>
        </tfoot>
      </table>
      <?php 
      if($expenseStatus != 1){
    echo '<div class="formButtons">';
    echo "<button class=\"btn btn-default\" type=\"submit\" name=\"action\" value=\"submit\">Update</button>";
    echo "<button class=\"btn btn-default\" type=\"submit\" name=\"action\" value=\"close\">Update and Close</button>";
    echo '</div>';
    }
  echo '</form>';
echo '</div>';

       
} // form end
include_once("inc/footer.inc.php");  