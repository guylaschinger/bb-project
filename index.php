<?php
// start or resume session
if (!isset($_SESSION)) {
    session_start();
}

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');
$currentModule = "";

// load page parts
$currentPage = "The Hub";
$currentModule = 1;
require_once("inc/header.inc.php");
require_once("inc/navbar.inc.php");
// Clear error message
$errorMsg = "";
$lastProjectSearch = $_SESSION['lastProjectSearch'];
//pre tags
echo "<br/>";

echo "<p>UserId: ".$userId."</p><br/>";
echo "<p>Current Module: ".$currentModule."</p><br/>";
echo "<p>Current Page: ".$currentPage."</p><br/>";
echo "<p>Last Project Search: ".$lastProjectSearch."</p><br/>";

include_once("inc/footer.inc.php");  