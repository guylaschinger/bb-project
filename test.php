<html>
<head>
<script type="text/javascript">
// Send the year value to create the make pop up menu
function showExpenseType(expenseType)
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('subType').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","show_type.php?expensetype=" + expenseType, true);
xmlhttp.send();
}
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
// Remove the hidden attribute of the submit button when the user chooses the model value from the pop up menu

$(document).ready(function() {
$("#subType").change(function(event) {
        X = event.target.value;
        if (X == 0) {
            $("#submit").attr("hidden", "hidden");
        } else {
            $("#submit").removeAttr("hidden", "hidden");
        }    
    });
});
</script>

<?php
// Connect to the database
// Connect to the database
define("ENVIRONMENT", "development");
// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');

//Get the Year value list
$layout =& $fm->getLayout('tbl_expensetype');
$values = $layout->getValueListTwoFields('expenseTypeList');

$expensePopup = '<select name="expenseType" id="year" onChange="showExpenseType(this.value)">';
$expensePopup .= '<option value=""></option>';

foreach($values as $key => $value)
{
  $expensePopup .= '<option value="' . $value . '">' . $key . '</option>';  
}

$expensePopup .= '<select>';
?>

</head>
<body>

<form action="related_value_list.php" method="get" id="form">
<?php echo '<label>Expense Type: </label>' . $expensePopup; 
// Replace the contents of the make div tag with the content returned from the showMake JavaScript code (ie. contents from the show_make.php page).
echo '<div id="subType"></div>';
?>
<div><input name="submit" type="Submit" value="Submit" id="submit" hidden="hidden"></div>
</form>

</body>
</html>

