<?php
//--------------------settings-----------------------------------------------
if (!isset($_SESSION)) {
    session_start();
}
// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");
// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');
// load page parts
$currentPage = "Project";
$currentModule = 3;
$realName = $_SESSION['realName'];
$userId = $_SESSION['userId'];
$userRecId = $_SESSION['userRecId'];
if(!isset($_SESSION['recId'])) {
 $recId = null; 
}else{
  $recId = $_SESSION['recId'];
}
 //$recId = null;
$searchString = null; 
$lastProjectSearch = $_SESSION['lastProjectSearch']; 
if(empty($lastProjectSearch)) { $lastProjectSearch = "15-002"; }
if(!isset($_GET['skip'])) {
    $skip = 0;
}else{
    $skip = $_GET['skip'];
}
$max = 20;
// gather info
$recId = null;
if(isset($_GET['recId'])){
    $recId = $_GET['recId'];
}elseif(isset($_POST['recId'])){
    $recId = $_POST['recId'];
}else($recId = null);
$searchString = null;
require_once("inc/header.inc.php");
require_once("inc/navbar.inc.php");

// value list handler function
function printCheckBox($layoutName, $fieldName, $recId, $type, $direction='v') {
    global $fm, $record;
    $fieldValue = $record->getField($fieldName);
    $layoutObj = $fm->getLayout($layoutName);
    $valuearray = $layoutObj->getValueListTwoFields($fieldName,$recId);
    $out = "";
    foreach ($valuearray as $displayValue => $value) {
        if(strpos($fieldValue, $value) !== FALSE){
            $checked = ' checked ';
        }else {
            $checked = ' ';
        }
        $out .= ($direction == 'h') ? '<span class="floatLeft labelCheckbox">' : '';
        $out .= '<label><input type="' .$type. '" name="' . $fieldName . '" value="' . $value . '"' . $checked . '/>' . $displayValue . '</label>';
        $out .= ($direction == 'v') ? '<br/>' : '';
        $out .= ($direction == 'h') ? '</span>' : '';
    }
    echo $out;
}

// Clear error message
$errorMsg = "";

if(isset($_GET['mode'])){
    $mode = $_GET['mode'];
}else{
    $mode = null;
}
if(isset($_GET['stageNew'])){
    $stageNew = $_GET['stageNew'];
}else{
    $stageNew = 1;
}
$submitSearch = null;

// OPTION A yes submitSearch and no recid and no mode -> search result - list view
// OPTION B  mode new -> form view and clear recId - form view
// OPTION C  mode edit-> form view and recId set  - form view with data
// OPTION D  no submitSearch and no recId and no mode-> last search - list view
// OPTION E  no submitSearch and yes recId -> display view

//--------------------A START -----------------------------------------------
if(isset($_POST['submitSearch'])){
    $option =  'A';
    $searchString = $_POST["navSearch"];
    if($searchString == ""){
        $searchString = $lastProjectSearch;  
    }
    $find_command = $fm->newCompoundFindCommand('tbl_project');
        $findreq =& $fm->newFindRequest('tbl_project');
        $findreq2 =& $fm->newFindRequest('tbl_project');
        $findreq3 =& $fm->newFindRequest('tbl_project');
        $findreq4 =& $fm->newFindRequest('tbl_project');
        $findreq5 =& $fm->newFindRequest('tbl_project');
        $findreq->addFindCriterion('jobNumberDisplay', $searchString);
        $findreq2->addFindCriterion('tbl_project|clientCompany::companyName', $searchString);
        $findreq3->addFindCriterion('jobName', $searchString);
        $findreq4->addFindCriterion('jobLocation', $searchString);
        $findreq5->addFindCriterion('currentStatus', $searchString);
        $find_command->add(1,$findreq);
        $find_command->add(2,$findreq2);
        $find_command->add(3,$findreq3);
        $find_command->add(4,$findreq4);
        $find_command->add(5,$findreq5);
        $find_command->setRange($skip, $max);
        $find_command->addSortRule('pk_projectID', 1, FILEMAKER_SORT_DESCEND);
        $result = $find_command->execute();
        // If an error is found, return a message and exit.
            if (FileMaker::isError($result)) {
                echo "Error: " . $result->getMessage(). "<br>";
                include_once("inc/footer.inc.php");  
                exit;
            }

                $records = $result->getRecords();
                $record = $records[0];
                $_SESSION['lastProjectSearch'] = $searchString;
                $found = $result->getFoundSetCount();
                $prev = $skip - $max;
                $next = $skip + $max;
                if(($skip + $max) > $found) {$next = $skip; }
?>
    <div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
            <th>Job No.</th>
            <th>Company</th>
            <th>Job Name</th>
            <th>Location</th>
            <th>Status</th>
            </tr>
        </thead>
        <tbody>
        
        <?php
        foreach($result->getRecords() as $row){
            echo "<tr>";
            echo "<td><a href=\"".$_SERVER['PHP_SELF']."?recId=" . $row->getRecordId() . "\">" . $row->getField('jobNumberDisplay') . "</td>";
            echo "<td>" . $row->getField('tbl_project|clientCompany::companyName') . "</td>";
            echo "<td>" . $row->getField('jobName') . "</td>";
            echo "<td>" . $row->getField('jobLocation') . "</td>";
            echo "<td>" . $row->getField('currentStatus') . "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
        </table>
    </div>
<?php
if($found > $max){
        require_once("inc/pagination.inc.php");
    }
// update last project search
    $data = array('lastProjectSearch'=>$lastProjectSearch);
    $newEdit = $fm->newEditCommand('tbl_user', $userRecId, $data); 
    $result = $newEdit->execute();
}//--------------------A END -----------------------------------------------
//--------------------B START -----------------------------------------------
elseif($mode != null){
    if($mode == 'edit'){
    $option =  'B';
    $recId = $_GET['recId'];
    $_SESSION['recId'] = $recId;
    /*echo "<pre>";
    echo 'SESSION: '.print_r($_SESSION);
    echo "</pre>"; */
    if($recId != null){
    $record = $fm->getRecordById('tbl_project', $_GET['recId']);
    if (FileMaker::isError($record)) {
    echo "<body>Error: " . $record->getMessage(). "</body>";
    exit;
    }
    foreach ($record as $records) {
                    $jobNumberDisplay = $records->getField('jobNumberDisplay');
                    $jobName = $record->getField('jobName');
                    $jobLocation = $record->getField('jobLocation');
                    $notes = $record->getField('notes');
                    $clientCompany = $record->getField('tbl_project|clientCompany::companyName');
                    $clientCompanyAddress = $record->getField('tbl_project|clientCompany::companyAddressBlock');
                    $division = $record->getField('tbl_project|division::divisionName');
                    $group = $record->getField('group');
                    $director = $record->getField('tbl_project|directorUser::realName');
                    $assocDirector = $record->getField('tbl_project|assocDirectorUser::realName');
                    $jobArchitect = $record->getField('tbl_project|jobArchitectUser::realName');
                    $currentStage = $record->getField('currentStage');
                    $currentStatus = $record->getField('currentStatus');
                    $sector = $record->getField('sector');
                    $tenant = $record->getField('tenant');
                    $siteArea = $record->getField('siteArea');
                    $buildingSizeSqm = $record->getField('buildingSizeSqm');
                    $buildingValue = $record->getField('buildingValue');
                    $overallProjectValue = $record->getField('overallProjectValue');
                    $constructionValue = $record->getField('constructionValue');
                    $dataLocation = $record->getField('dataLocation');
                    $contract = $record->getField('contract');
                    $region = $record->getField('region');
                    $siteLocation = $record->getField('siteLocation');
                    $alsoKnownAs = $record->getField('alsoKnownAs');
                    $projectEmail = $record->getField('projectEmail');
                    $email_access_username = $record->getField('email_access_username');
                    $email_access_password = $record->getField('email_access_password');
                    $projectContactCount = $record->getField('projectContactCount');
                    $projectHours = $record->getField('projectHours');
                    $projectExpenses = $record->getField('projectExpenses');
                    
                    
    }
}


echo "mode: {$mode}.<br/>";

    ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Edit Project Details</h3>
      </div>
      <div class="panel-body">
        <form class="form-horizontal" form action="inc/datahandler_project.php" method="post">
            <div class="form-group">
            <label class="col-sm-2 control-label" for "projectNumber">Project Number</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php 
                    if(empty($jobNumberDisplay)){
                        echo "The project number will be assigned once the record is commited.";
                    }else{
                      echo $jobNumberDisplay;
                      } ?></p>
            </div>
          </div>
            <div class="form-group">
            <label class="col-sm-2 control-label" for "projectName">Project Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="jobName" placeholder="Project Name" value="<?php echo $jobName ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "jobLocation">Location</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="jobLocation" placeholder="Location" value="<?php echo $jobLocation ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="client" class="col-sm-2 control-label">Client</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $clientCompany ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "clientAddress">Address</label>
            <div class="col-sm-10">
              <p class="form-control-static"><?php echo $clientCompanyAddress ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "division">Division</label>
            <div class="col-sm-10">
              <?php
              if(empty($recId)){
                ?>
                <input type="checkbox" name="divisionName" value="3" /> BBA <br />
                <input type="checkbox" name="divisionName" value="4" /> BBBS <br />
                <input type="checkbox" name="divisionName" value="5" /> BBAS <br />
                <?php
              }else{
                printCheckBox('tbl_project', 'divisionName', $recId, 'radio', 'v'); 
              }
               ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "studio">Studio</label>
            <div class="col-sm-10">
                <?php
              if(empty($recId)){
                ?>
                <input type="checkbox" name="group" value="London" /> London <br />
                <input type="checkbox" name="group" value="Sheffield" /> Sheffield<br />
                <input type="checkbox" name="group" value="Westerham" /> Westerham<br />
                <?php
              }else{
                printCheckBox('tbl_project', 'group', $recId, 'radio', 'v'); 
              }
               ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "director">Director</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="director" placeholder="Director" value="<?php echo $director ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "assocDirector">Associate Director</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="assocDirector" placeholder="Assoc Director" value="<?php echo $assocDirector ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "jobArchitect">Job Architect</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="jobArchitect" placeholder="Job Architect" value="<?php echo $jobArchitect ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for "currentStage">Current Stage</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="currentStage" placeholder="Current Stage" value="<?php echo $currentStage ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "currentStatus">Current Status</label>
            <div class="col-sm-10">
                <?php
              if(empty($recId)){
                ?>
                <input type="checkbox" name="currentStatus" value="Current" /> Current <br />
                <input type="checkbox" name="currentStatus" value="Archive" /> Archive <br />
                <input type="checkbox" name="currentStatus" value="Void" /> Void <br />
                <?php
              }else{
                printCheckBox('tbl_project', 'currentStatus', $recId, 'radio', 'v'); 
              }
               ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "sector">Sector</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="sector" placeholder="Sector" value="<?php echo $sector ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "tenant">Tenant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="tenant" placeholder="Tenant" value="<?php echo $tenant ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "siteArea">Site Area</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="siteArea" placeholder="Site Area" value="<?php echo $siteArea ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "buildingSizeSqm">Building Size Sqm</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="buildingSizeSqm" placeholder="Building Size Sqm" value="<?php echo $buildingSizeSqm ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "buildingValue">Building Value</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="buildingValue" placeholder="£" value="<?php echo $buildingValue ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "overallProjectValue">Overall Project Value</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="overallProjectValue" placeholder="£" value="<?php echo $overallProjectValue ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "constructionValue"> Construction Value </label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="constructionValue" placeholder="£" value="<?php echo $constructionValue ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "dataLocation"> Data Location </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="dataLocation" placeholder="Data ocation" value="<?php echo $dataLocation ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "contract"> Contract </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="contract" placeholder="Contract" value="<?php echo $contract ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "region"> Region </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="region" placeholder="Region" value="<?php echo $region ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "siteLocation"> Site Location </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="siteLocation" placeholder="Site Location " value="<?php echo $siteLocation ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "alsoKnownAs"> Also Known As </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="alsoKnownAs " placeholder="alsoKnownAs" value="<?php echo $alsoKnownAs ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "projectEmail"> Project Email </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="projectEmail" placeholder="Project Email" value="<?php echo $projectEmail ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "email_access_username"> Email Access Username </label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="email_access_username" placeholder="Email Access Username" value="<?php echo $email_access_username ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for "email_access_password"> Email Access Password </label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="email_access_password" placeholder="Email Access Password" value="<?php echo $email_access_password ?>">
            </div>
          </div>            

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="hidden" name="recId" value="<?php echo $recId; ?>">
                <input type="hidden" name="mode" value="Edit">
                <input type="hidden" name="currentModule" value="<?php echo $currentModule; ?>">  
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          </div>
        </form> 
        
    </div> <!-- panel-body END DIV -->
</div> <!---panel panel-default END DIV -->
<?php
//--------------------B END -----------------------------------------------
//--------------------C START -----------------------------------------------
    //<!---newStage 1 START-->
    }elseif($mode == 'new'){
        $option =  'B';
        //echo "mode: {$mode}.<br/>";
        //echo "stageNew: {$stageNew}.<br>";
            if($stageNew == 1){
                ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">New Project Step 1 - Project Name</h3>
                  </div>
                  <div class="panel-body">
                  <form class="form-horizontal" form action="inc/datahandler_project.php" method="post">
                    <div class="form-group">
                    <label class="col-sm-2 control-label" for "projectName">Project Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="jobName" placeholder="Project Name" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for "jobLocation">Location</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="jobLocation" placeholder="Location" value="">
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-2 control-label" for "division">Division</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="divisionName" value="3" /> BBA <br />
                        <input type="checkbox" name="divisionName" value="4" /> BBBS <br />
                        <input type="checkbox" name="divisionName" value="5" /> BBAS <br />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for "studio">Studio</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="group" value="London" /> London <br />
                        <input type="checkbox" name="group" value="Sheffield" /> Sheffield<br />
                        <input type="checkbox" name="group" value="Westerham" /> Westerham<br />
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <input type="hidden" name="stageNew" value="1">
                        <input type="hidden" name="mode" value="New">
                        <input type="hidden" name="fk_userIdCreatedBy" value="<?php echo $userId; ?>">
                        <input type="hidden" name="createdBy" value="<?php echo $$realName; ?>">
                        <input type="hidden" name="currentModule" value="<?php echo $currentModule; ?>">  
                      <button type="submit" class="btn btn-default">Next</button>
                    </div>
                  </div>
                  </form>
                  </div> <!-- panel-body END DIV -->  
                </div><!---panel panel-default END DIV -->
            <!---newStage 1 END-->
            <!---newStage 2 START-->
        <?php
            }elseif ($stageNew == 2) { 
                $record = $fm->getRecordById('tbl_project', $_GET['recId']);
                if (FileMaker::isError($record)) {
                    echo "<body>Error: " . $record->getMessage(). "</body>";
                    exit;
                    }
                foreach ($record as $records) {
                    $jobNumberDisplay = $records->getField('jobNumberDisplay');
                    $jobName = $record->getField('jobName');
                }
                ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">New Project Step 2 - Search for Project Client</h3>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" form action="<?php echo $_SERVER['PHP_SELF'].'?mode=new&stageNew=3&recId='.$recId; ?>" method="post">
                        <div class="form-group">
                        <label class="col-sm-2 control-label" for "clientName">Project Name</label>
                            <div class="col-sm-10">
                                <span class="form-group-text"><?php echo $jobNumberDisplay ?> Project: <?php echo $jobName ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for "clientName">Client Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="clientName" placeholder="Client Company Name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <input type="hidden" name="stageNew" value="3">
                                <input type="hidden" name="mode" value="New">
                                <input type="hidden" name="searchType" value="client"> 
                              <button type="submit" class="btn btn-default">Next</button>
                            </div>
                        </div>
                    </form>
                  </div> <!-- panel-body END DIV -->  
                </div><!---panel panel-default END DIV -->
                <?php
                //<!---newStage 2 END-->
                //<!---newStage 3 START-->
                }elseif($stageNew == 3 ){
                    $clientName = $_POST['clientName'];
                    $find_command = $fm->newFindCommand('tbl_company');
                    $find_command->addFindCriterion('companyName', $clientName);
                    $result = $find_command->execute();
                    if (FileMaker::isError($result)) {
                    $errorMsg = $result->getMessage();
                    } else {
                        ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title">New Project Step 3 - Select the Project Client</h3>
                          </div>
                          <div class="panel-body">
                            <form class="form-horizontal" form action="<?php echo $_SERVER['PHP_SELF'].'?mode=new&stageNew=4&recId='.$recId; ?>" method="post">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="result-header">
                                            <tr>
                                            <th class="col-sm-6">Client Name</th>
                                            <th class="col-sm-2">&nbsp;</th>
                                            <th class="col-sm 2">&nbsp;</th>
                                            </tr>
                                        </thead>
                                            <tbody class="result-scroll">
                                            <?php
                                            foreach($result->getRecords() as $row){
                                                echo "<tr>";
                                                echo "<td class=\"col-sm-2\">" . $row->getField('companyName') . "</td>";
                                                echo "<td>" . $row->getField('companyAddressString') . "</td>";
                                                echo "<td><input type=\"checkbox\" name=\"pk_companyID\" value=\"". $row->getField('pk_companyID') ."\" /></td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-11">
                                        <input type="hidden" name="stageNew" value="4">
                                        <input type="hidden" name="mode" value="New">
                                        <input type="hidden" name="searchType" value="client"> 
                                      <button type="submit" class="btn btn-default">Next</button>
                                    </div>
                                </div>
                            </form>
                            </div> <!-- panel-body END DIV -->  
                        </div><!---panel panel-default END DIV -->
                        <?php
                }
        //<!---newStage 3 END-->
        //<!---newStage 4 START-->
            }elseif ($stageNew == 4) { 
                $fk_companyID = $_POST['pk_companyID'];
                $recId = $_GET['recId'];
                $find_command = $fm->newFindCommand('tbl_contact_company');
                $find_command->addFindCriterion('fk_companyID', $fk_companyID);
                $result = $find_command->execute();
                if (FileMaker::isError($result)) {
                $errorMsg = $result->getMessage();
                }        
                ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">New Project Step 4 - Select Client Contact </h3>
                  </div>
                  <div class="panel-body">
                  <form class="form-horizontal" form action="inc/datahandler_project.php" method="post">
                    <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="result-header">
                                            <tr>
                                            <th class="col-sm-11">Client Contact</th>
                                            <th class="col-sm 1">&nbsp;</th>
                                            </tr>
                                        </thead>
                                            <tbody class="result-scroll">
                                            <?php
                                            foreach($result->getRecords() as $row){
                                                echo "<tr>";
                                                echo "<td class=\"col-sm-10\">" . $row->getField('tbl_contact_company|contact::nameCombinedLineString') . "</td>";
                                                echo "<td><input type=\"checkbox\" name=\"fk_contactID\" value=\"". $row->getField('fk_contactID') ."\" /></td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-11">
                                        <input type="hidden" name="stageNew" value="4">
                                        <input type="hidden" name="mode" value="New">
                                        <input type="hidden" name="fk_companyID" value="<?php echo $fk_companyID;?> ">
                                        <input type="hidden" name="recId" value="<?php echo $recId;?>"> 
                                      <button type="submit" class="btn btn-default">Next</button>
                                    </div>
                                </div>
                  </form>
                  </div> <!-- panel-body END DIV -->  
                </div><!---panel panel-default END DIV -->
        <?php
        //<!---newStage 4 END-->
        //<!---newStage 5 START-->
            }elseif ($stageNew == 5) {
                $recId = $_GET['recId'];
                $find_command = $fm->newFindCommand('tbl_user');
                $find_command->addFindCriterion('valid', 1);
                $findCommand->addSortRule('validSortKey', 1, FILEMAKER_SORT_ASCEND);
                $result = $find_command->execute();
                if (FileMaker::isError($result)) {
                $errorMsg = $result->getMessage();
                } 
//echo "<pre>";
    //echo 'GET: '.print_r($_GET);
    //echo 'SESSION: '.print_r($_SESSION);
    //echo 'POST: '.print_r($_POST);
    //print_r($result);
    //echo "</pre>"; 

            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">New Project Step 5 - Select Project Staff <?php echo "recId: {$recId}" ?> </h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" form action="inc/datahandler_project.php" method="post">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="result-header">
                                    <tr>
                                        <th class="col-sm-3">Name</th>
                                        <th class="col-sm 2">Office</th>
                                        <th class="col-sm 2">Job Title</th>
                                        <th class="col-sm 3">Project Role</th>
                                        <th class="col-sm 2">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody class="result-scroll">
                                    <?php
                                        foreach($result->getRecords() as $row){
                                            echo "<tr>";
                                            echo "<td class=\"col-sm-3\">" . $row->getField('realName') . "</td>";
                                            echo "<td class=\"col-sm-2\">" . $row->getField('office') . "</td>";
                                            echo "<td class=\"col-sm-3\">" . $row->getField('jobTitle') . "</td>";
                                            echo "<td class=\"col-sm-2\"></td>";
                                            echo "<td class=\"col-sm-2\"> </td>";
                                            echo "<td><input type=\"checkbox\" name=\"fk_contactID\" value=\"". $row->getField('pk_contactID') ."\" /></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-11">
                                <input type="hidden" name="stageNew" value="5">
                                <input type="hidden" name="mode" value="New">
                                <input type="hidden" name="fk_companyID" value="<?php echo $fk_companyID;?> ">
                                <input type="hidden" name="recId" value="<?php echo $recId;?>"> 
                              <button type="submit" class="btn btn-default">Next</button>
                            </div>
                        </div>
                    </form>
                </div> <!-- panel-body END DIV -->  
            </div><!---panel panel-default END DIV -->
            <?php
        //<!---newStage 4 END-->
        //<!---newStage 4 START-->
            } 
    } // END IF MODE NEW
} // END MODE SET
//--------------------C END -----------------------------------------------
//--------------------D START -----------------------------------------------
elseif($recId == null){
    $option =  'D';
    if($searchString == ""){
        $searchString = $lastProjectSearch;  
    }
        $find_command = $fm->newCompoundFindCommand('tbl_project');
        $findreq =& $fm->newFindRequest('tbl_project');
        $findreq2 =& $fm->newFindRequest('tbl_project');
        $findreq3 =& $fm->newFindRequest('tbl_project');
        $findreq4 =& $fm->newFindRequest('tbl_project');
        $findreq5 =& $fm->newFindRequest('tbl_project');
        $findreq->addFindCriterion('jobNumberDisplay', $searchString);
        $findreq2->addFindCriterion('tbl_project|clientCompany::companyName', $searchString);
        $findreq3->addFindCriterion('jobName', $searchString);
        $findreq4->addFindCriterion('jobLocation', $searchString);
        $findreq5->addFindCriterion('currentStatus', $searchString);
        $find_command->add(1,$findreq);
        $find_command->add(2,$findreq2);
        $find_command->add(3,$findreq3);
        $find_command->add(4,$findreq4);
        $find_command->add(5,$findreq5);
        $find_command->setRange($skip, $max);
        $find_command->addSortRule('pk_projectID', 1, FILEMAKER_SORT_DESCEND);
        $result = $find_command->execute();
        // If an error is found, return a message and exit.
            if (FileMaker::isError($result)) {
                echo "Error: " . $result->getMessage(). "<br>";
                include_once("inc/footer.inc.php");  
                exit;
            }

                $records = $result->getRecords();
                $record = $records[0];
                $_SESSION['lastProjectSearch'] = $searchString;
                $found = $result->getFoundSetCount();
                $prev = $skip - $max;
                $next = $skip + $max;
                if(($skip + $max) > $found) {$next = $skip; }
?>
    <div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
            <th>Project No.</th>            
            <th>Project Name</th>
            <th>Company</th>
            <th>Location</th>
            <th>Status</th>
            </tr>
        </thead>
        <tbody>
        
        <?php
        foreach($result->getRecords() as $row){
            echo "<tr>";
            echo "<td><a href=\"".$_SERVER['PHP_SELF']."?recId=" . $row->getRecordId() . "\">" . $row->getField('jobNumberDisplay') . "</td>";            
            echo "<td>" . $row->getField('jobName') . "</td>";
            echo "<td>" . $row->getField('tbl_project|clientCompany::companyName') . "</td>";
            echo "<td>" . $row->getField('jobLocation') . "</td>";
            echo "<td>" . $row->getField('currentStatus') . "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
        </table>
    </div>
<?php
if($found > $max){
        require_once("inc/pagination.inc.php");
    }
// update last project search
    $data = array('lastProjectSearch'=>$lastProjectSearch);
    $newEdit = $fm->newEditCommand('tbl_user', $userRecId, $data); 
    $result = $newEdit->execute();
}
//--------------------D END -----------------------------------------------
//--------------------E START -----------------------------------------------
elseif($recId != null){
    $option =  'E';
    $_SESSION['recId'] = $recId;
    // update last project search
    $data = array('lastProjectSearch'=>$lastProjectSearch);
    $newEdit = $fm->newEditCommand('tbl_user', $userRecId, $data); 
    $result = $newEdit->execute();
        $record = $fm->getRecordById('tbl_project', $_GET['recId']);
    if (FileMaker::isError($record)) {
    echo "<body>Error: " . $record->getMessage(). "</body>";
    exit;
    }
    foreach ($record as $records) {
                    $jobNumberDisplay = $records->getField('jobNumberDisplay');
                    $jobName = $record->getField('jobName');
                    $jobLocation = $record->getField('jobLocation');
                    $notes = $record->getField('notes');
                    $clientCompany = $record->getField('tbl_project|clientCompany::companyName');
                    $clientCompanyAddress = $record->getField('tbl_project|clientCompany::companyAddressBlock');
                    $division = $record->getField('tbl_project|division::divisionName');
                    $group = $record->getField('group');
                    $director = $record->getField('tbl_project|directorUser::realName');
                    $assocDirector = $record->getField('tbl_project|assocDirectorUser::realName');
                    $jobArchitect = $record->getField('tbl_project|jobArchitectUser::realName');
                    $currentStage = $record->getField('currentStage');
                    $currentStatus = $record->getField('currentStatus');
                    $sector = $record->getField('sector');
                    $tenant = $record->getField('tenant');
                    $siteArea = $record->getField('siteArea');
                    $buildingSizeSqm = $record->getField('buildingSizeSqm');
                    $buildingValue = $record->getField('buildingValue');
                    $overallProjectValue = $record->getField('overallProjectValue');
                    $constructionValue = $record->getField('constructionValue');
                    $dataLocation = $record->getField('dataLocation');
                    $contract = $record->getField('contract');
                    $region = $record->getField('region');
                    $siteLocation = $record->getField('siteLocation');
                    $alsoKnownAs = $record->getField('alsoKnownAs');
                    $projectEmail = $record->getField('projectEmail');
                    $email_access_username = $record->getField('email_access_username');
                    $email_access_password = $record->getField('email_access_password');
                    $projectContactCount = $record->getField('projectContactCount');
                    $projectHours = $record->getField('projectHours');
                    $projectExpenses = $record->getField('projectExpenses');
                    
                    
    }
    ?>
<div class="row">
    <div class="col-xs-9">
        <form class="form-inline">
            <div class="form-group">
              <label for="jobNumber">Project Number</label>
              <p><?php echo $jobNumberDisplay; ?></p>
            </div>
            <div class="form-group">
              <label for="jobName">Project Name</label>
              <p><?php echo $jobName ; ?></p>
            </div>
        </form>
    </div>
</div>
<div class="bs-example">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sectionA">Project Details</a></li>
        <li><a data-toggle="tab" href="#sectionB">Project Details Extended</a></li>
        <li><a data-toggle="tab" href="#sectionC">Directory</a></li>
        <li><a data-toggle="tab" href="#sectionD">Notes</a></li>
        <li><a data-toggle="tab" href="#sectionE">Hours / Fees</a></li>
    </ul>
    <div class="tab-content">
        <div id="sectionA" class="tab-pane fade in active"><!-- Project Details Start -->
            <div class="row">
            <!-- leftBox     -->
                <div class="col-md-6">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for "client">Client</label>
                        <div class="col-sm-6">
                          <p class="form-control-static"><?php echo $clientCompany ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for "clientAddress">Address</label>
                        <div class="col-sm-6">
                          <p class="form-control-static"><?php echo $clientCompanyAddress ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for "jobLocation">Location</label>
                        <div class="col-sm-6">
                          <p class="form-control-static"><?php echo $jobLocation ?></p>
                        </div>
                      </div>
                    </form> 
                </div>
                <!-- rightBox     -->
                <div class="col-md-6">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"for "division">Division</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $division ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"for "studio">Studio</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $group ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"for "director">Director Responsible</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $director ?></p>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 control-label"for "assocDirector">Associate Responsible</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $assocDirector ?></p>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 control-label"for "jobArchitect">Job Architect</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $jobArchitect ?></p>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 control-label"for "currentStatus">Current Status</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $currentStatus?></p>
                            </div>
                        </div>
                         <div class="form-group">
                        <label class="col-sm-4 control-label"for "currentStage">Current Stage</label>
                        <div class="col-sm-6">
                          <p class="form-control-static"><?php echo $currentStage?></p>
                        </div>
                      </div>
                    </form>
                </div>
            </div> <!--row end-->
        </div><!-- Project Details End -->
        <div id="sectionB" class="tab-pane fade"><!-- Project Details Extended Start -->
            <h3>Project Details Extended</h3>
                <div class="row">
                    <!-- leftBox     -->
                    <div class="col-md-6"><!--project detail extended start -->
                        <form class="form-horizontal">
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "sector">Sector</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $sector ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "tenant">Tenant</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $tenant ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "siteArea">Site Area</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $siteArea ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "buildingSize">Building Size</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $buildingSizeSqm ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "buildingValue">Building Value</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $buildingValue ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "overallProjectValue">Overall Project Value</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $overallProjectValue ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "dataLocation">Data Location</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $dataLocation ?></p>
                            </div>
                          </div>
                        </form> 
                    </div> <!--project detail extended end -->

                    <!-- rightBox     -->
                    <div class="col-md-6"><!--project detail extended right start -->
                        <form class="form-horizontal">
                            <div class="form-group">
                            <label class="col-sm-4 control-label" for "constructionValue">Construction Value</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $constructionValue ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "contract">Contract</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $contract ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "region">Region</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $region ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "siteLocation">Site Location</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $siteLocation ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "alsoKnownAs">Also Known As</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $alsoKnownAs ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "projectEmail">Project Email</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $projectEmail ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "email_access_username">Email Access UserName</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $email_access_username ?></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4 control-label" for "email_access_password">Email Access Password</label>
                            <div class="col-sm-6">
                              <p class="form-control-static"><?php echo $email_access_password ?></p>
                            </div>
                          </div>
                        </form>
                    </div> <!--project detail extended right end -->
                  </div><!--row end-->
            </div><!-- Project Details Extended End -->
        <div id="sectionC" class="tab-pane fade"><!-- Directory Start -->
            <h3>Directory</h3>
                <div class="row">
                    <!-- leftBox      -->
                    <div class="col-md-8">
                      <?php echo 'count:'. $projectContactCount ?>
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Job Title</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                                if(($projectContactCount) >0 ) {
                                $contactData=$record->getRelatedSet('tbl_project|project_contact');
                                foreach ($contactData as $contactRow) {
                                       $contactName = $contactRow->getField('tbl_project|project_contact::contactName');
                                       $jobTitle =$contactRow->getField('tbl_project|project_contact::jobTitle');
                                       $mobile =$contactRow->getField('tbl_project|project_contact::mobile');
                                       $email =$contactRow->getField('tbl_project|project_contact::email');
                                       $recId= $contactRow->getRecordId(); 
                                       echo '<tr>';
                                       echo "<td><a href=\"contacts.php?recId=" . $recId . "\">" . $contactName . "</td>";
                                       echo '<td>'.$jobTitle.'</td><td>'.$mobile.'</td><td>'.$email;
                                       echo '</td></tr>';
                                        }
                                        echo '</table>';
                                }else{
                                    echo '<p>Not attached to any project directories</p>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- rightBox ----------------------------       -->
                    <div class="col-md-4">
                      <p>search thingum goes here</p>
                    </div>
                </div>
        </div><!-- Directory End -->
        <div id="sectionD" class="tab-pane fade"><!-- Notes Start -->
            <h3>Notes</h3>
                <div class="row">
                    <!-- leftBox -----------------------------      -->
                    <div class="col-md-4">
                    </div>
                    <!-- rightBox ----------------------------       -->
                    <div class="col-md-8">
                    </div>
                </div>
        </div><!-- Notes End -->
        <div id="sectionE" class="tab-pane fade"><!-- Hours/Fees Start -->
        
                <div class="row">
                    <!-- leftBox -----------------------------      -->
                    <div class="col-md-6">
                      <h3>Project Hours</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Week Date</th>
                                    <th>Task</th>
                                    <th>Hours</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                                if(($projectHours) >0 ) {
                                $contactData=$record->getRelatedSet('tbl_project|timesheetlineitem');
                                foreach ($contactData as $contactRow) {
                                       $realName = $contactRow->getField('tbl_project|timesheetlineitem|timesheet|user::realName');
                                       $date =$contactRow->getField('tbl_project|timesheetlineitem|timesheet::date');
                                       $stdtask =$contactRow->getField('tbl_project|timesheetlineitem|stdtask::taskName');
                                       $weekTotal =$contactRow->getField('tbl_project|timesheetlineitem::weekTotal');
                                       $recId= $contactRow->getRecordId(); 
                                       echo '<tr>';
                                       echo '<td>'.$realName.'</td><td>'.$date.'</td><td>'.$stdtask.'</td><td>'.$weekTotal;
                                       echo '</td></tr>';
                                        }
                                        echo '<tr>&nbsp</tr><tr>&nbsp</tr><tr>&nbsp</tr><tr>'.$projectHours.'</tr>';
                                        echo '</table>';
                                }else{
                                    echo '<p>No project time has been recorded</p>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- rightBox ----------------------------       -->
                    <div class="col-md-6">
                      <h3>Project Expenses</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Qty</th>
                                    <th>Unit Cost</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                                if(($projectExpenses) >0 ) {
                                $contactData=$record->getRelatedSet('tbl_project|project_expense');
                                foreach ($contactData as $contactRow) {
                                       $description = $contactRow->getField('tbl_project|project_expense::description');
                                       $type =$contactRow->getField('tbl_project|project_expense::type');
                                       $qty =$contactRow->getField('tbl_project|project_expense::qty');
                                       $unitcost =$contactRow->getField('tbl_project|project_expense::unitcost');
                                       $amount =$contactRow->getField('tbl_project|project_expense::amount');
                                       $recId= $contactRow->getRecordId(); 
                                       echo '<tr>';
                                       echo '<td>'. $description .'</td><td>'. $type . '</td><td>'.$qty.'</td><td>'.$unitcost.'</td><td>'.$amount.'</td></tr>';
                                        }
                                        echo '<tr>&nbsp</tr><tr>&nbsp</tr><tr>&nbsp</tr><tr>&nbsp</tr><tr>'.$projectExpenses.'</tr>';
                                        echo '</table>';
                                }else{
                                    echo '<p>No project expenses have been recorded</p>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div><!-- Hours/Fees End -->
        
    </div>
</div>
<?php

}//--------------------E END -----------------------------------------------

//--------------------query-----------------------------------------------
// - -  query by recId - - - - - - - - - - - 

include_once("inc/footer.inc.php");
