<?php
// start or resume session
if (!isset($_SESSION)) {
    session_start();
}

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');

// load page parts
$currentPage = "Contact";
$currentModule = 2;
$realName = $_SESSION['realName'];
$userId = $_SESSION['userId'];
$userRecId = $_SESSION['userRecId'];
$lastContactSearch = $_SESSION['lastContactSearch']; 
if(empty($lastContactSearch)) { $lastContactSearch = "Bond Bryan"; }
if(!isset($_GET['skip'])) {
    $skip = 0;
}else{
    $skip = $_GET['skip'];
}
$max = 20;
$recId = '';
require_once("inc/header.inc.php");
require_once("inc/navbar.inc.php");
// Clear error message
$errorMsg = "";
//--------------------submitSearch[GET]------------------------------------------------

if(isset($_GET["mode"])){
    $mode = $_GET["mode"];
    echo "getMode {$mode}";
     ?>
    <h1>New Contact Form</h1>
    <div class="row">
        <div class="col-md-6">.col-md-6</div>
        <div class="col-md-6">.col-md-6</div>
    </div>
    <?php

//--------------------recId [GET]------------------------------------------------
//--------------------timeSheetList------------------------------------------------ 
}else{if(isset($_GET["recId"])){
    $recId = $_GET['recId'];
    // update last contact search
    $data = array('lastContactSearch'=>$lastContactSearch);
    $newEdit = $fm->newEditCommand('tbl_user', $userRecId, $data); 
    $result = $newEdit->execute();

    $record = $fm->getRecordById('tbl_contact', $_GET['recId']);
    if (FileMaker::isError($record)) {
    echo "<body>Error: " . $record->getMessage(). "</body>";
    exit;
    }
        foreach ($record as $records) {
                    $nameCombined = $records->getField('nameCombined');
                    $companyName = $record->getField('tbl_contact|contact_company|company::companyName');
                    $address1 = $record->getField('tbl_contact|contact_company|company::address1');
                    $address2 = $record->getField('tbl_contact|contact_company|company::address2');
                    $address3 = $record->getField('tbl_contact|contact_company|company::address3');
                    $address4 = $record->getField('tbl_contact|contact_company|company::address4');
                    $city = $record->getField('tbl_contact|contact_company|company::city');
                    $county = $record->getField('tbl_contact|contact_company|company::county');
                    $postcode = $record->getField('tbl_contact|contact_company|company::postcode');
                    $country = $record->getField('tbl_contact|contact_company|company::country');
                    $tel = $record->getField('tbl_contact|contact_company|company::tel');
                    $fax = $record->getField('tbl_contact|contact_company|company::fax');
                    $url = $record->getField('tbl_contact|contact_company|company::url');
                    $directTel = $records->getField('directTel');
                    $mobile = $records->getField('mobile');
                    $email = $records->getField('email');
                    $jobTitle = $records->getField('jobTitle');
                    $maidenName = $records->getField('maidenName');
                    $directTel = $record->getField('directTel');
                    $salutation = $record->getField('salutation');
                    $projectNumber = $record->getField('projectNumber');
                    
                    //build address
                    $addressLine1 = (!empty($address1)?"{$address1}, ":'');
                    $addressLine1 .= (!empty($address2)?"{$address2}, ":'');
                    $addressLine1 .= (!empty($address3)?"{$address3}, ":'');
                    $addressLine1 .= (!empty($address4)?"{$address4}, ":'');
                    $addressLine2 = (!empty($city)?"{$city}, ":'');
                    $addressLine2 .= (!empty($county)?"{$county}, ":'');
                    $addressLine2 .= (!empty($postcode)?"{$postcode}, ":'');
                    $addressLine2 .= (!empty($country)?"{$country}, ":'');

        }        
    ?>
    <h1>Contact Detail Form</h1>
    
    <div class="row">
        <!-- leftBox -----------------------------      -->
        <div class="col-md-4">
            <address>
                <strong><?php echo $nameCombined; ?></strong><br>
                <?php echo $companyName; ?><br>
                <?php echo $addressLine1; ?><br>
                <?php echo $addressLine2 ?><br>
                <a href="<?php echo $url ?>"><?php echo $url ?></a><br>
                <abbr title="Phone">P:</abbr> <?php echo $tel ?><br>
                <abbr title="Fax">F:</abbr> <?php echo $fax ?><br>
                <abbr title="Direct">D:</abbr> <?php echo $directTel ?><br>
                <abbr title="Mobile">M:</abbr> <?php echo $mobile ?><br>
                <a href="mailto:#"><?php echo $email ?></a>
            </address>
        </div>
        <!-- rightBox ----------------------------       -->
        <div class="col-md-8">
            
                                <h3>Projects</h3>
                                <?php
                                if(($projectNumber) >0) {
                                $contactData=$record->getRelatedSet('tbl_contact|project_contact|project');
                                echo '<td><table>';
                                foreach ($contactData as $contactRow) {
                                       $jobNumberDisplay = $contactRow->getField('tbl_contact|project_contact|project::jobNumberDisplay');
                                       $jobName =$contactRow->getField('tbl_contact|project_contact|project::jobName');
                                       $companyName =$contactRow->getField('tbl_contact|project_contact|project|company::companyName');
                                       $divisionName =$contactRow->getField('tbl_contact|project_contact|project|division::divisionName');
                                       $recId= $contactRow->getRecordId(); 
                                       echo '<tr>';
                                       echo "<td><a href=\"projects.php?recId=" . $recId . "\">" . $jobNumberDisplay . "</td>";
                                       echo '<td>'.$jobName.' '.$companyName.' '.$divisionName;
                                       echo '</td></tr>';
                                        }
                                        echo '</table>';
                                }else{
                                    echo '<p>Not attached to any projects</p>';
                                }
                                ?>
        
        </div>
    </div>
    <?php
//--------------------submitSearch[POST]------------------------------------------------
//--------------------Contact List------------------------------------------------ 
}else{if(isset($_POST["submitSearch"])){
    $searchString = $_POST["navSearch"];
    }elseif(isset($_GET["searchString"]))
    $searchString = $_GET["searchString"];
    else{
    $searchString = $lastContactSearch;  
    }
    $find_command = $fm->newCompoundFindCommand('tbl_contact');
    $findreq =& $fm->newFindRequest('tbl_contact');
    $findreq2 =& $fm->newFindRequest('tbl_contact');
    $findreq3 =& $fm->newFindRequest('tbl_contact');
    $findreq4 =& $fm->newFindRequest('tbl_contact');
    $findreq->addFindCriterion('firstName', $searchString);
    $findreq2->addFindCriterion('lastName', $searchString);
    $findreq3->addFindCriterion('tbl_contact|contact_company|company::companyName', $searchString);
    $findreq4->addFindCriterion('nameCombined', $searchString);
    $find_command->add(1,$findreq);
    $find_command->add(2,$findreq2);
    $find_command->add(3,$findreq3);
    $find_command->add(4,$findreq4);
    $find_command->setRange($skip, $max);
    $find_command->addSortRule('lastName', 1, FILEMAKER_SORT_ASCEND);
    $result = $find_command->execute();
    // If an error is found, return a message and exit.
        if (FileMaker::isError($result)) {
            echo "Error: " . $result->getMessage(). "<br>";
            exit;
        }
            $records = $result->getRecords();
            $record = $records[0];
            $_SESSION['lastContactSearch'] = $searchString;
            $found = $result->getFoundSetCount();
            $prev = $skip - $max;
            $next = $skip + $max;
            if(($skip + $max) > $found) {$next = $skip; }
      
      
            
     ?>
    <table>
        <thead>
            <th><?php echo "Found " . $found . " results for '".$searchString ."'"?></th>
        </thead>
        <tbody>
           <?php
                
                //get and display each Question-Answer pair as a new table row
                foreach ($records as $record) {
                    $nameCombined = $record->getField('nameCombined');
                    $companyName = $record->getField('tbl_contact|contact_company|company::companyName');
                    $companyNameString = "";
                    
                    //converts any line returns in the answer to commas
                    $nameCombined = str_replace("\n",", ",$nameCombined);
                    // companyname string
                    if(!empty($companyName)) {$companyNameString = ' of '.$companyName;}else{echo '&nbsp';}
                    
                    echo '<tr><td><a href='.$_SERVER['PHP_SELF'].'?recId=' . $record->getRecordId() . '>' . $nameCombined .'</a>'. $companyNameString . '</td></tr>';
                    
                }
            ?> 
        </tbody>
    </table>
    <?php
    if($found > $max){
        require_once("inc/pagination.inc.php");
    }
    }
}

include_once("inc/footer.inc.php");  