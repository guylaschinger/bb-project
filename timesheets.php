<?php
// start or resume session
if (!isset($_SESSION)) {
    session_start();
}

// FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS  FUNCTIONS 
// value list handler function
function printDropList($layoutName, $valuelistName, $listName, $recId) {
  global $fm, $record;
  $layoutObj = $fm->getLayout($layoutName);
  $valuearray = $layoutObj->getValueListTwoFields($valuelistName,$recId);
  $out = '<select name="'. $listName .'" >';
  foreach ($valuearray as $displayValue => $value) {
    $out .= '<option value="' . $value . '">' . $displayValue . '</option>';
  }
  $out .= '</select>';
  echo $out;
}
// date handler function
function toggleDMY2MDY($datein){
    if(strlen($datein)>0){
        $parts=explode('/',$datein);
        return$parts[1].'/'.$parts[0].'/'.$parts[2];
    }
}
// complete status display function
function completeStatus($completeStatus){
    if( $completeStatus == 1 ) {
        $statusDisplay = 'Completed';
    } else {
        $statusDisplay = '';
    }
        return $statusDisplay;
}
// timeSheet display function
function timeSheetRowDisplay($completeStatus, $day, $dayValue){
    if( $completeStatus == 1 ) {
        $timeSheetRowDisplay =  '<td class="td-narrow">'. $dayValue .'</td>';
    } else {
        $timeSheetRowDisplay = '<td class="td-narrow"><input type="text" id="'. $day .'" name="'. $day .'[]" value="'. $dayValue .'"></td>';
            }
                return $timeSheetRowDisplay;
        }


// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
$timeSheetUser = $_SESSION['timeSheetUser'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');
$currentModule = "";

// load page parts
$currentPage = "Time Sheet";
$currentModule = 4;
$recId = '';
require_once("inc/header.inc.php");
require_once("inc/navbar.inc.php");

$recId = null;
if(isset($_GET['recId'])){
    $recId = $_GET['recId'];
}elseif(isset($_POST['recId'])){
    $recId = $_POST['recId'];
}else($recId = null);
// Clear error message
$errorMsg = "";
if($timeSheetUser != 1){
    echo 'No time sheet required';
    //--------------------timeSheetList------------------------------------------------ 
}elseif($recId == null){
    $findCommand =& $fm->newFindCommand('tbl_timesheet');
    $findCommand->addFindCriterion('fk_userID', $userId);
    $findCommand->addSortRule('pk_timeSheetID', 1, FILEMAKER_SORT_DESCEND);
    $record = $findCommand->execute();
    if (FileMaker::isError($record)) {
    echo "<body>Error: " . $record->getMessage(). "</body>";
    exit;
    }
    ?>
        <div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
            <th>No.</th>
            <th>Date</th>
            <th>Hours Recorded</th>
            <th>Completed On</th>
            <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($record->getRecords() as $records){
                echo "<tr>";
                echo "<td><a href=\"".$_SERVER['PHP_SELF']."?recId=" . $records->getRecordId() . "\">" . $records->getField('pk_TimeSheetID') . "</td>";
                echo "<td>" . toggleDMY2MDY($records->getField('date')) . "</td>";
                echo "<td class=\"align-right\">" . $records->getField('timeSheetTotal') . "</td>";
                echo "<td>" . toggleDMY2MDY($records->getField('completeDate')) . "</td>";
                echo "<td>" . completeStatus($records->getField('completeStatus')) . "</td>";
                echo "</tr>";
                }
            ?>
        </tbody>
        </table>
    </div>

<?php
    //--------------------timeSheetForm------------------------------------------------ 
}else{
    $recId = $_GET['recId'];
    $record = $fm->getRecordById('tbl_timesheet', $_GET['recId']);
        if (FileMaker::isError($record)) {
        echo "  <body>Error: " . $record->getMessage(). "</body>";
        exit;
    }
    foreach ($record as $records) {
                $timeSheetID = $records->getField('pk_TimeSheetID');
                $userID = $records->getField('fk_userID');
                $userName = $records->getField('tbl_timesheet|user::realName');
                $date = $records->getField('date');
                $completeStatus = $records->getField('completeStatus');
                $completeDate = $records->getField('completeDate');
                $mondayTotal = $records->getField('mondayTotal');
                $tuesdayTotal = $records->getField('tuesdayTotal');
                $wednesdayTotal = $records->getField('wednesdayTotal');
                $thursdayTotal = $records->getField('thursdayTotal');
                $fridayTotal = $records->getField('fridayTotal');
                $saturdayTotal = $records->getField('saturdayTotal');
                $sundayTotal = $records->getField('sundayTotal');
                $timeSheetTotal = $records->getField('timeSheetTotal'); 
                $timeSheetRowCount = $records->getField('timeSheetRowCount');  
    }
   ?>
<!-- Time Sheet Detail-->
<div class="row">
    <!-- leftBox -----------------------------      -->
    <div class="col-md-6">
        <?php echo "TimeSheet Date: " . toggleDMY2MDY($records->getField('date'))  ?><br>
        <?php echo "Name: {$userName}"; ?><br>
    </div>
    <!-- rightBox -->
    <div class="col-md-6">
        <?php echo "TimeSheet No.: {$timeSheetID}"; ?><br>
        <?php echo "Completion Date: ". toggleDMY2MDY($records->getField('completeDate')) ?><br>
    </div>      
</div>
<div class="table-responsive">
    <form id="timesheet-form" action="inc/datahandler_timesheet.php" method="post">
      <table class="table table-condensed">
        <thead>
          <tr>
            <th class="th-expenseProject">Project</th>
            <th>Task</th>
            <th>Mon</th>
            <th>Tues</th>
            <th>Wed</th>
            <th>Thurs</th>
            <th>Fri</th>
            <th>Sat</th>
            <th>Sun</th>
            <th>Total</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            <?php
            if($timeSheetRowCount > 0){
                $findCommand =& $fm->newFindCommand('tbl_timesheetlineitem');
                $findCommand->addFindCriterion('fk_timeSheetID', $timeSheetID);
                $record = $findCommand->execute();
                if (FileMaker::isError($record)) {
                echo "<body>Error: " . $record->getMessage(). "</body>";
                exit;
            }
               foreach($record->getRecords() as $records){
                $fk_projectID = $records->getField('fk_projectID');
                $fk_standardTaskID = $records->getField('fk_standardTaskID');
                $monday = $records->getField('monday');
                $tuesday = $records->getField('tuesday');
                $wednesday = $records->getField('wednesday');
                $thursday = $records->getField('thursday');
                $friday = $records->getField('friday');
                $saturday = $records->getField('saturday');
                $sunday = $records->getField('sunday');
                $rowRecId = $records->getField('recId');
                
                echo "<tr>";
                echo '<td class="th-expenseProject">' . $records->getField('description') . "</td>";
                echo '<td>' . $records->getField('tbl_timesheetlineitem|stdtask::taskName') . "</td>";
                echo timeSheetRowDisplay($completeStatus, 'monday', $monday);
                echo timeSheetRowDisplay($completeStatus, 'tuesday', $tuesday);
                echo timeSheetRowDisplay($completeStatus, 'wednesday', $wednesday);
                echo timeSheetRowDisplay($completeStatus, 'thursday', $thursday);
                echo timeSheetRowDisplay($completeStatus, 'friday', $friday);
                echo timeSheetRowDisplay($completeStatus, 'saturday', $saturday);
                echo timeSheetRowDisplay($completeStatus, 'sunday', $sunday);
                echo "<td>" . $records->getField('weekTotal') . "</td>";
                echo '<input type="hidden" id="fk_projectID" name="fk_projectID[]" value=' . $fk_projectID . '>';
                echo '<input type="hidden" id="rowRecId" name="rowRecId[]" value=' . $rowRecId . '>';
                echo '<input type="hidden" id="fk_standardTaskID" name="fk_standardTaskID[]" value=' . $fk_standardTaskID . '>';
                echo '<td class="deleteBox"><input type="checkbox" id="delete" name="delete_'.$rowRecId.'" value="1"></td>';
                echo "</tr>";
                }
                // blank row
                if($completeStatus == 0){
                echo '<tr><td class="projectDescription" id="fk_projectID" name="fk_projectID[]">';
                printDropList('tbl_timesheet', 'timesheetProjects', 'fk_projectID[]', $recId);
                echo '</td><td id="timesheetStdTasks" name="timesheetStdTasks[]">';
                printDropList('tbl_timesheet', 'timesheetStdTasks', 'fk_standardTaskID[]', $recId);
                echo "</td>";
                echo '<td class="td-narrow"><input type="text" id="monday" name="monday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="tuesday"  name="tuesday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="wednesday"  name="wednesday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="thursday"  name="thursday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="thursday" name="friday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="saturday" name="saturday[]" value=""></td>';
                echo '<td class="td-narrow"><input type="text" id="sunday" name="sunday[]" value=""></td>';
                echo '<input type="hidden" id="rowRecId" name="rowRecId[]" value="">';
                echo "</tr>";
                } //end blank row
           }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td><?php echo $mondayTotal ?></td>
                <td><?php echo $tuesdayTotal ?></td>
                <td><?php echo $wednesdayTotal ?></td>
                <td><?php echo $thursdayTotal ?></td>
                <td><?php echo $fridayTotal ?></td>
                <td><?php echo $saturdayTotal ?></td>
                <td><?php echo $sundayTotal ?></td>
                <td><?php echo $timeSheetTotal ?></td>
            </tr>
        </tfoot>
      </table>
    <?php
    echo "<input type=\"hidden\" name=\"recId\" value=\"{$recId}\">";
    echo "<input type=\"hidden\" name=\"currentModule\" value=\"44\">";
     echo "<input type=\"hidden\" name=\"fk_timeSheetID\" value=\"{$timeSheetID}\">";
    if($completeStatus != 1){
    echo '<div class="formButtons">';
    echo "<button class=\"btn btn-default\" type=\"submit\" name=\"action\" value=\"submit\">Update</button>";
    echo "<button class=\"btn btn-default\" type=\"submit\" name=\"action\" value=\"close\">Update and Close</button>";
    echo '</div>';
    }
    echo '</form>';
echo '</div>';
}// form end


include_once("inc/footer.inc.php");  