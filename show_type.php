<?php
if (!isset($_SESSION)) {
    session_start();
}

// Connect to the database
define("ENVIRONMENT", "development");
// load config and library tools
require_once('config/initialise.php');
require_once('config/config_local.php');

// Find the record based on the year passed in the URL
$request = $fm->newFindCommand('tbl_expense_subtype');
$request->addFindCriterion('fk_expenseTypeID', $_GET['expensetype']);
$result = $request->execute();
$records = $result->getRecords();
/*echo "<pre>";
print_r($records);
echo "</pre>";
exit;*/

// Create an array with just the make values
// Could you just place a portal on the layout that shows the related values and skip this step to make a unique array?
// Yes. However, that places more load on FileMaker to create a relationship, display a portal on the layout, and send 
// all that extra data to the web server. If I have a need for speed I would rather have the web server do the heavy 
// lifting and spare FileMaker since it's the weakest link!
foreach($records as $record)
{
	$subTypes[ $record->getField('pk_expenseSubTypeID') ] = $record->getField('name');
}

// echo "<pre>";
// print_r($subTypes);
// echo "</pre>";
// exit;

// Remove duplicate values from the array
//$types = array_unique($subTypeID);

// Format the results for a new pop up menu.
$menuType = '<select name="Type">';
$menuType .= '<option value=""></option>';

foreach( $subTypes as $type => $name) {
	$menuType .= '<option value="' . $type . '">' . $name . '</option>';	
}

$menuType .= '</select>';

// Return the new pop up menu
echo '<Label>Type: </Label>' . $menuType;

?>