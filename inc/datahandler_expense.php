<?php
if (!isset($_SESSION)) {
    session_start();
}
// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
define("ENVIRONMENT", "development");

// load config and library tools
require_once('../config/initialise.php');
require_once('../config/config_local.php');

    echo "<pre>";
    // echo 'GET: '.print_r($_GET);
    // echo 'SESSION: '.print_r($_SESSION);
    echo 'POST: '.print_r($_POST);
    // print_r($record);
    echo "</pre>"; 
    exit;

    $norecords=count($_POST['fk_projectID']);

    for($i=0; $i < $norecords; $i++){

    	$test = '' . $_POST['monday'][$i];
    	$test .= $_POST['tuesday'][$i];
    	$test .= $_POST['wednesday'][$i];
    	$test .= $_POST['thursday'][$i];
    	$test .= $_POST['friday'][$i];
    	$test .= $_POST['saturday'][$i];
    	$test .= $_POST['sunday'][$i];

    	if ( $test <> '' ) {
	    	if ( $_POST['rowRecId'][$i] == '' ) {
                $values =   array(
                                'fk_timeSheetID' => $_POST['fk_timeSheetID'],
                                'fk_projectID' => $_POST['fk_projectID'][$i],
                                'fk_standardTaskID' => $_POST['fk_standardTaskID'][$i],
                                'monday' => $_POST['monday'][$i],
                                'tuesday' => $_POST['tuesday'][$i],
                                'wednesday' => $_POST['wednesday'][$i],
                                'thursday' => $_POST['thursday'][$i],
                                'friday' => $_POST['friday'][$i],
                                'saturday' => $_POST['saturday'][$i],
                                'sunday' => $_POST['sunday'][$i]
                            );

                $rec = $fm->createRecord('tbl_timesheetlineitem', $values);
                $result = $rec->commit();
                if (FileMaker::isError($rec)) {
                echo 'Record addition failed:: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
                exit;
                }

	    	} else {
	    		$values =   array(
                                'fk_timeSheetID' => $_POST['fk_timeSheetID'],
                                'fk_projectID' => $_POST['fk_projectID'][$i],
                                'fk_standardTaskID' => $_POST['fk_standardTaskID'][$i],
                                'monday' => $_POST['monday'][$i],
                                'tuesday' => $_POST['tuesday'][$i],
                                'wednesday' => $_POST['wednesday'][$i],
                                'thursday' => $_POST['thursday'][$i],
                                'friday' => $_POST['friday'][$i],
                                'saturday' => $_POST['saturday'][$i],
                                'sunday' => $_POST['sunday'][$i]
                            );

                $newEdit = $fm->newEditCommand('tbl_timesheetlineitem', $_POST['rowRecId'][$i], $values);
                $result = $newEdit->execute();
                if (FileMaker::isError($rec)) {
                echo 'Record addition failed:: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
                exit;
                }
	    	}
    	} else {
    		echo "<p>IGNORE i={$i} RecID={$_POST['rowRecId'][$i]}</p>";

    	}
    }
    header("Location: ../timesheets.php?recId={$_POST['recId']}");