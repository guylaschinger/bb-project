
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">The Hub</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if($currentModule == 2) echo 'class="active"'?>><a href="contacts.php">Contacts</a></li>
            <li <?php if($currentModule == 3) echo 'class="active"'?>><a href="projects.php">Projects</a></li>
            <li <?php if($currentModule == 4) echo 'class="active"'?>><a href="timesheets.php">TimeSheets</a></li>
            <li <?php if($currentModule == 5) echo 'class="active"'?>><a href="expenses.php">Expenses</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Action <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <?php
                    if($currentModule > 1) {
                        echo '<li><a href="'.$_SERVER['PHP_SELF'].'?mode=new">New '.$currentPage.'</a></li>';}
                    if(isset($_GET['recId'])){
                        echo '<li><a href="'.$_SERVER['PHP_SELF'].'?mode=edit&recId='.$recId.'">Edit '.$currentPage.' |recId: '.$recId.'</a></li>'; 
                        }

                ?>
              </ul>
            </li>
            <li>
              <?php
                if ($currentModule > 1){
                  echo '<form class="navbar-form navbar-right" method="post" action="'.$_SERVER['PHP_SELF'].'">';
                  echo '<input type="text" name="navSearch" class="form-control" placeholder="Search ' .$currentPage.'">';
                  echo '</li><li><button type="submit" name="submitSearch" class="btn btn-btn">Search</button></li>';
                  echo '</form>';
                  }
                ?>
            </li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container">
    