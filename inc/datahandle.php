<html>
<head>
<title>Add/Edit Record</title>
</head>
<?php
/**
 * handleForm.php
 * 
 * Copyright © 2005-2006, FileMaker, Inc. All rights reserved.
 * NOTE: Use of this source code is subject to the terms of the FileMaker
 * Software License which accompanies the code. Your use of this source code
 * signifies your agreement to such license terms and conditions. Except as
 * expressly granted in the Software License, no other copyright, patent, or
 * other intellectual property license or right is granted, either expressly or
 * by implication, by FileMaker.
 *
 * This is a script to handle the form submit for a new record or editing a record.
 */

// Turn on output buffering so that we can set Location: HTTP Header later on
ob_start();
if (!isset($_SESSION)) {
    session_start();
}

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('../config/initialise.php');
require_once('../config/config_local.php');

// Create FileMaker object
// Location of FileMaker Server is assumed to be on the same machine,
//  thus we assume hostspec is api default of 'http://localhost' as specified
//  in filemaker-api.php.
// If FMSA web server is on another machine, specify 'hostspec' as follows:
//   $fm = new FileMaker('FMPHP_Sample', 'http://10.0.0.1');


if(isset($_GET['currentModule'])){

	    /*echo "<pre>";
	    echo 'GET: '.print_r($_GET);
	    echo 'SESSION: '.print_r($_SESSION);
	    echo 'POST: '.print_r($_POST);
	    print_r($record);
	    echo "</pre>"; 
	    echo "test";
	    exit;*/

if($_GET['currentModule'] == 4) {
	$currentModule = $_GET['currentModule'];
	$dataTable = 'tbl_timesheet';	
	$pk_ID = 'pk_TimeSheetID';
	$date = ($_GET['date'] + 86400);
	$fk_userID = $_GET['fk_userID'];
	$keys = array(
			'date',
			'fk_userID'
			);
	}elseif ($_GET['currentModule'] == 5) {
	$dataTable = 'tbl_expense';
	$pk_ID = 'pk_expenseID';
	$keys = array(
			'fk_userID'
			);
	}

	// utility function to set field values from posted data
	function setFieldData($record)
	{
		// declare $keys as a global variable
		global $keys;
		// loop over each field value and append to array
		$result = array();
		foreach ($keys as $fieldname) {
			$value = null;
			// workaround PHP's insistence that spaces in
			// form variables be replaced by "_"
			if (!strpos($fieldname, " ")) {
				$value = $_GET[$fieldname];
			} else {
				$value = $_GET[str_replace(" ", "_", $fieldname)];
			}
			if (strlen($value) > 0) {
				$record->setField($fieldname, $value);
			} elseif (strlen($record->getField($fieldname)) > 0) {
				$record->setField($fieldname, null);
			}
		}
		return $result;
	}
	// declare $rec
	$rec = null;

	// check to see that user didn't hit 'cancel' button
	if (!array_key_exists('cancel', $_GET)) {
		// Check for recid parameter which determines if this is a create new or edit
		if (array_key_exists('recId', $_GET)) {
			$rec = $fm->getRecordById($dataTable, $_POST['recId']);
		} else {
			$rec =& $fm->createRecord($dataTable);
		}
		if (FileMaker::isError($rec)) {
		    echo 'Record addition failed:: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
		    exit;
		}
		// set field data from form data
		setFieldData($rec);
		// commit record to database
		$result = $rec->commit();
		if (FileMaker::isError($result)) {
		    echo 'Record Addition Failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
		    exit;
		}
           
		$recId = $rec->_impl->_recordId;
		$pk_TimeSheetID = $rec->getField('pk_TimeSheetID');

		$data = array('fk_timeSheetID'=>$pk_TimeSheetID);
        $newAdd = $fm->newAddCommand('tbl_timesheetlineitem', $data); 
        $result = $newAdd->execute();
        if (FileMaker::isError($result)) {
		    echo 'Record Addition Failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
		    exit;
		}
		
	}

	// set Location: HTTP header to force redirect
	if($_GET['currentModule'] == 4) {
	header("Location: ../timesheets.php?recId={$recId}");
	}elseif ($_GET['currentModule'] == 5) {
	header("Location: ../expenses.php?recId={$recId}");
	}

}else{
	// POST   POST   POST   POST   POST   POST   POST   POST   POST   POST   POST   POST   POST  	
	// Field names we expect as keys in $_POST[]
	if($_POST['currentModule'] == 3) {
	$dataTable = 'tbl_project';
	$keys = array(
	        'jobName',
	        'jobLocation',
	        'notes',
	        'clientCompany',
	        'clientCompanyAddress',
	        'division',
	        'group',
	        'director',
	        'assocDirector',
	        'jobArchitect',
	        'currentStage',
	        'currentStatus',
	        'sector',
	        'tenant',
	        'siteArea',
	        'buildingSizeSqm',
	        'buildingValue',
	        'overallProjectValue',
	        'constructionValue',
	        'dataLocation',
	        'contract',
	        'region',
	        'siteLocation',
	        'alsoKnownAs',
	        'projectEmail',
	        'email_access_username',
	        'email_access_password',
	        'projectContactCount',
	        'projectHours',
	        'projectExpenses'
			);
	}elseif ($_GET['currentModule'] == 4) {
	$currentModule = $_GET['currentModule'];
	$dataTable = 'tbl_timesheet';	
	$date = $_GET['date'];
	$keys = array(
			'date',
			'fk_userID'
			);
	}elseif ($_POST['currentModule'] == 44) {
	$dataTable = 'tbl_timesheetlineitem';
	$keys = array(
			'monday',
			'tuesday',
			'wednesday',
			'thursday',
			'friday',
			'saturday',
			'sunday'
			);
	}elseif ($_POST['currentModule'] == 5) {
	$dataTable = 'tbl_expense';
	$keys = array(
			'fk_userID'
			);
	}

	    /*echo "<pre>";
	    echo 'GET: '.print_r($_GET);
	    echo 'SESSION: '.print_r($_SESSION);
	    echo 'POST: '.print_r($_POST);
	    print_r($record);
	    echo "</pre>"; 
	    exit;*/

	// utility function to set field values from posted data
	function setFieldData($record)
	{
		// declare $keys as a global variable
		global $keys;
		// loop over each field value and append to array
		$result = array();
		foreach ($keys as $fieldname) {
			$value = null;
			// workaround PHP's insistence that spaces in
			// form variables be replaced by "_"
			if (!strpos($fieldname, " ")) {
				$value = $_POST[$fieldname];
			} else {
				$value = $_POST[str_replace(" ", "_", $fieldname)];
			}
			if (strlen($value) > 0) {
				$record->setField($fieldname, $value);
			} elseif (strlen($record->getField($fieldname)) > 0) {
				$record->setField($fieldname, null);
			}
		}
		return $result;
	}
	// declare $rec
	$rec = null;



	// check to see that user didn't hit 'cancel' button
	if (!array_key_exists('cancel', $_POST)) {
		// Check for recid parameter which determines if this is a create new or edit
		if (array_key_exists('recId', $_POST)) {
			$rec = $fm->getRecordById($dataTable, $_POST['recId']);
		} else {
			$rec =& $fm->createRecord($dataTable);
		}
		if (FileMaker::isError($rec)) {
		    echo 'Record addition failed:: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
		    exit;
		}
		// set field data from form data
		setFieldData($rec);
		// commit record to database
		$result = $rec->commit();


		if (FileMaker::isError($result)) {
		    echo 'Record Addition Failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
		    exit;
		}
	}

	// set Location: HTTP header to force redirect
	/*if($_POST['currentModule'] == 3) {
	header("Location: ../projects.php?recId={$_POST['recId']}");
	}elseif ($_POST['currentModule'] == 4) {
	header("Location: ../timesheets.php?recId={$_POST['recId']}");
	}elseif ($_POST['currentModule'] == 44) {
	header("Location: ../timesheets.php?recId={$_POST['recId']}");
	}elseif ($_POST['currentModule'] == 5) {
	header("Location: ../expenses.php?recId={$_POST['recId']}");
	}*/
}

// End output buffering and flush output
ob_end_flush();
?>
