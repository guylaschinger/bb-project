<html>
<head>
<title>Add/Edit Record</title>
</head>
<?php

ob_start();
if (!isset($_SESSION)) {
    session_start();
}

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('../config/initialise.php');
require_once('../config/config_local.php');

// Create FileMaker object
// Location of FileMaker Server is assumed to be on the same machine,
//  thus we assume hostspec is api default of 'http://localhost' as specified
//  in filemaker-api.php.
// If FMSA web server is on another machine, specify 'hostspec' as follows:
//   $fm = new FileMaker('FMPHP_Sample', 'http://10.0.0.1');


// Field names we expect as keys in $_POST[]
if($_POST['currentModule'] == 3) {
$dataTable = 'tbl_project';
$keys = array(
		'jobLocation',
		'group',
		'currentStage',
		'currentStatus',
		'sector',
		'tenant',
		'siteArea',
		'buildingSizeSqm',
		'buildingValue'
		);
}elseif ($_POST['currentModule'] == 4) {
$dataTable = 'tbl_timesheet';
$keys = array(
		'jobLocation',
		);
}elseif ($_POST['currentModule'] == 44) {
$dataTable = 'tbl_timesheetlineitem';
$keys = array(
		'fk_projectID',
		'fk_standardTaskID',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday',
		'sunday',
		);
}


    /*echo "<pre>";
    //echo 'GET: '.print_r($_GET);
    echo 'SESSION: '.print_r($_SESSION);
    echo 'POST: '.print_r($_POST);
    //print_r($record);
    echo "</pre>"; 
    exit;*/

// utility function to set field values from posted data
function setFieldDataArray($record, $index)
{
	// declare $keys as a global variable
	global $keys;
	// loop over each field value and append to array
	foreach ($keys as $fieldname) {
		$value = null;
		// workaround PHP's insistence that spaces in
		// form variables be replaced by "_"
		if (!strpos($fieldname, " ")) {
			$value = $_POST[$fieldname][$index];
		} else {
			$value = $_POST[str_replace(" ", "_", $fieldname)][$index];
		}
		if (strlen($value) > 0) {
			$record->setField($fieldname, $value);
		} elseif (strlen($record->getField($fieldname)) > 0) {
			$record->setField($fieldname, null);
		}
	}
}
// declare $rec
$rec = null;

// check to see that user didn't hit 'cancel' button
if (!array_key_exists('cancel', $_POST)) {
	// Check for recid parameter which determines if this is a create new or edit
	$i = 0;
	$recIdArray = $_POST['rowRecId'];
	//testing goes here

	//print_r($recIdArray);
	//exit;

	//testing ends here
	foreach ($recIdArray as $currRecId) {
			// test for day data
		$checkVars = array($recIdArray);
			if(in_array($recIdArray, $checkVars)){
				 print_r($checkVars);
			}
			exit;	

		$rec = $fm->getRecordById($dataTable, $currRecId);
		if (FileMaker::isError($rec)) {
		    echo 'Record addition failed:: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
		    exit;
		}
		// set field data from form data
		setFieldDataArray($rec, $i);
		// commit record to database
		$result = $rec->commit();

		if (FileMaker::isError($result)) {
		    echo 'Record Addition Failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
		    exit;
		}
		$i += 1;
		//echo 'increment: '. $i . '<br>';
		//exit;
	}
	
}


// set Location: HTTP header to force redirect
if($_POST['currentModule'] == 3) {
header("Location: ../projects.php?recId={$_POST['recId']}");
}elseif ($_POST['currentModule'] == 4) {
header("Location: ../timesheets.php?recId={$_POST['recId']}");
}elseif ($_POST['currentModule'] == 44) {
header("Location: ../timesheets.php?recId={$_POST['recId']}");
}


// End output buffering and flush output
ob_end_flush();
?>
