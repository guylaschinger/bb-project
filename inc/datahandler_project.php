<html>
<head>
<title>Add/Edit Record</title>
</head>
<?php


// Turn on output buffering so that we can set Location: HTTP Header later on
ob_start();
if (!isset($_SESSION)) {
    session_start();
}

// Check if the user is authenticated and if not pass to login page
if (!isset($_SESSION['userId'])) {
    header("Location:login.php");
    exit;
}
$userId = $_SESSION['userId'];
// - The environment may be debug, development, testing & production
define("ENVIRONMENT", "development");

// load config and library tools
require_once('../config/initialise.php');
require_once('../config/config.php');
$dataTable = 'tbl_project';
if($_POST['mode'] == 'Edit'){
// Field names we expect as keys in $_POST[]
$keys = array(
        'jobLocation',
        'jobName',
        'notes',
        'division',
        'group',
        'currentStage',
        'currentStatus',
        'sector',
        'tenant',
        'siteArea',
        'buildingSizeSqm',
        'buildingValue',
        'overallProjectValue',
        'constructionValue',
        'dataLocation',
        'contract',
        'region',
        'siteLocation',
        'alsoKnownAs',
        'projectEmail',
        'email_access_username',
        'email_access_password'
		);

// utility function to set field values from posted data
function setFieldData($record)
{
	// declare $keys as a global variable
	global $keys;
	// loop over each field value and append to array
	$result = array();
	foreach ($keys as $fieldname) {
		$value = null;
		// workaround PHP's insistence that spaces in
		// form variables be replaced by "_"
		if (!strpos($fieldname, " ")) {
			$value = $_POST[$fieldname];
		} else {
			$value = $_POST[str_replace(" ", "_", $fieldname)];
		}
		if (strlen($value) > 0) {
			$record->setField($fieldname, $value);
		} elseif (strlen($record->getField($fieldname)) > 0) {
			$record->setField($fieldname, null);
		}
	}
	return $result;
}

	/*echo "<pre>";
    // echo 'GET: '.print_r($_GET);
    // echo 'SESSION: '.print_r($_SESSION);
    echo 'POST: '.print_r($_POST);
    // print_r($record);
    echo "</pre>"; 
    exit;*/

// declare $rec
$rec = null;

// check to see that user didn't hit 'cancel' button
if (!array_key_exists('cancel', $_POST)) {
	// Check for recid parameter which determines if this is a create new or edit
	if (array_key_exists('recId', $_POST)) {
		$rec = $fm->getRecordById($dataTable, $_POST['recId']);
	} else {
		$rec =& $fm->createRecord($dataTable);
	}
	if (FileMaker::isError($rec)) {
	    echo 'Record addition failed: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
	    exit;
	}
	// set field data from form data
	setFieldData($rec);
	// commit record to database
	$result = $rec->commit();
	
	if (FileMaker::isError($result)) {
	    echo 'Record addition failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
	    exit;
	}
}

// set Location: HTTP header to force redirect
header("Location: ../projects.php?recId={$_POST['recId']}");
// NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  
}elseif ($_POST['mode'] == 'New' and $_POST['stageNew'] == '1') {
	$newPerformScript = $fm->newPerformScriptCommand($dataTable, 'projects - new project number', session_id() );
	$result = $newPerformScript->execute();

	$findCommand = $fm->newFindCommand($dataTable); 
	$findCommand->addFindCriterion('sessionID', session_id() ); 
	$result = $findCommand->execute();
		if (FileMaker::isError($result)) {
	    echo "Error: " . $result->getMessage(). "<br>";
	    exit;
		}
		$records = $result->getRecords();
		foreach ($records as $record) {
			$recId = $record->getField('recId');
		}
	$keys = array(
        'jobLocation',
        'jobName',
        'notes',
        'division',
        'group',
        'sessionID'
    );

		// utility function to set field values from posted data
		function setFieldData($record)
		{
			// declare $keys as a global variable
			global $keys;
			// loop over each field value and append to array
			$result = array();
			foreach ($keys as $fieldname) {
				$value = null;
				// workaround PHP's insistence that spaces in
				// form variables be replaced by "_"
				if (!strpos($fieldname, " ")) {
					$value = $_POST[$fieldname];
				} else {
					$value = $_POST[str_replace(" ", "_", $fieldname)];
				}
				if (strlen($value) > 0) {
					$record->setField($fieldname, $value);
				} elseif (strlen($record->getField($fieldname)) > 0) {
					$record->setField($fieldname, null);
				}
			}
			return $result;
		}

		// declare $rec
		$rec = null;

			$rec = $fm->getRecordById($dataTable, $recId);

			if (FileMaker::isError($rec)) {
			    echo 'Record addition failed: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
			    exit;
			}
			// set field data from form data
			setFieldData($rec);
			// commit record to database
			$result = $rec->commit();
			
			if (FileMaker::isError($result)) {
			    echo 'Record addition failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
			    exit;
			}

// set Location: HTTP header to force redirect
header("Location: ../projects.php?mode=new&stageNew=2&recId={$recId}");
// NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  NEW  
}elseif ($_POST['mode'] == 'New' and $_POST['stageNew'] == '4') {

	$keys = array(
        'fk_contactID',
        'fk_companyID'
		);

// utility function to set field values from posted data
function setFieldData($record)
{
	// declare $keys as a global variable
	global $keys;
	// loop over each field value and append to array
	$result = array();
	foreach ($keys as $fieldname) {
		$value = null;
		// workaround PHP's insistence that spaces in
		// form variables be replaced by "_"
		if (!strpos($fieldname, " ")) {
			$value = $_POST[$fieldname];
		} else {
			$value = $_POST[str_replace(" ", "_", $fieldname)];
		}
		if (strlen($value) > 0) {
			$record->setField($fieldname, $value);
		} elseif (strlen($record->getField($fieldname)) > 0) {
			$record->setField($fieldname, null);
		}
	}
	return $result;
}

	/*echo "<pre>";
    // echo 'GET: '.print_r($_GET);
    // echo 'SESSION: '.print_r($_SESSION);
    echo 'POST: '.print_r($_POST);
    // print_r($record);
    echo "</pre>"; 
    exit;*/

// declare $rec
$rec = null;

// check to see that user didn't hit 'cancel' button
if (!array_key_exists('cancel', $_POST)) {
	// Check for recid parameter which determines if this is a create new or edit
	if (array_key_exists('recId', $_POST)) {
		$rec = $fm->getRecordById($dataTable, $_POST['recId']);
	} else {
		$rec =& $fm->createRecord($dataTable);
	}
	if (FileMaker::isError($rec)) {
	    echo 'Record addition failed: (' . $rec->getCode() . ') ' . $rec->getMessage() . "\n";
	    exit;
	}
	// set field data from form data
	setFieldData($rec);
	// commit record to database
	$result = $rec->commit();
	
	if (FileMaker::isError($result)) {
	    echo 'Record addition failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
	    exit;
	}
}
$recId = $_POST['recId'];
// set Location: HTTP header to force redirect
header("Location: ../projects.php?mode=new&stageNew=5&recId={$recId}");
}
// End output buffering and flush output
ob_end_flush();
?>
